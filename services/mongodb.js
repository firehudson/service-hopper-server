import mongoose from 'mongoose';
import bluebird from 'bluebird';

const service = {
  name: 'mongo db',

  initModels: () => {
  /* eslint-disable global-require */
    process.log('info', 'Initializing models');

    /**
     * require all models here, otherwise mongoose won't find them later.
     */
    require('../models/user');
    require('../models/authToken');
    require('../models/serviceCategory');
    require('../models/service');
  },

  start: async () => {
    process.log('info', `Starting service ${service.name}`);

    /**
     * mongoose default promise library is deprecated. Using bluebird instead.
     * http://mongoosejs.com/docs/promises.html
     */
    mongoose.Promise = bluebird;

    /**
     * Set up default mongoose connection.
     */
    await mongoose.connect(process.env.MONGO_DB_URL, { useMongoClient: true })
      .then(({ db: { databaseName } }) => {
        process.log('info', `Connected to database -> ${databaseName}`);
        process.log('info', `Successfully Started service ${service.name}`);
      })
      .catch((err) => {
        process.log('error', `Failed to start service ${service.name}`, err);
      });
  },

  stop: () => {
    process.log(`Stopping service ${service.name}`);

    /**
     * Disconnect all mongoose connection.
     */
    mongoose.disconnect()
      .then(() => process.log('info', `Successfully Stopped service ${service.name}`));
  },
};

export default service;
