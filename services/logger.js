import { Logger, transports } from 'winston';

/**
 * 
 *  Service hoppee logger service 
 * -------------------------------
 * 
 *  Usage -
 *    process.log(level, message, meta, callback);
 * 
 *    @params
 *    - level: String [error|info|warn]
 *    - message: String
 *    - meta?: Object
 *    - callback?: function
 */

const service = {
  name: 'logger',
  start: () => {
    console.log(`Starting service ${service.name}`);

    /**
     * defining custom logging levels.
     */
    // const levels = {
    //   error: 0,
    //   warn: 1,
    //   info: 2,
    //   api: 3,
    //   database: 4,
    //   services: 5,
    //   exception: 6,
    // };
    // 
    //
    //  TODO: implement custom logging levels for api, database and services.
    //


    /**
     * defining colors for custom levels.
     */
    const colors = {
      error: 'red',
      warn: 'yellow',
      info: 'blue',
      api: 'red',
      database: 'blue',
      services: 'yellow',
      exception: 'grey',
    };

    /**
     * file transporter to write logs to files.
     */
    const fileTransporter = new (transports.File)({
      filename: process.env.LOG_FILE_PATH,
    });

    /**
     * console transporter to write logs to onsole.
     */
    const consoleTransporter = new (transports.Console)({
      colorize: true,
    });

    /**
     * configure winston with transporters.
     */
    const logger = new Logger({
      colors,
      transports: [
        fileTransporter,
        consoleTransporter,
      ],
    });

    /**
     * assign logger to process, we need it all over the app.
     */
    process.log = (level, message, ...params) => {
      let callback;
      let meta;

      if (typeof level !== 'string') {
        logger.log('warn', 'Must provide a log level');
        throw new Error('Must provide a log level');
      }

      if (typeof message !== 'string') {
        logger.log('warn', 'Must provide a log message');
        throw new Error('Must provide a log message');
      }

      /**
       * mutating params to allow user to optionally pass metadata.
       */
      if (typeof params[0] === 'function') {
        meta = {};
        callback = params[0];
      } else {
        meta = params[0] || '';
        callback = params[1];
      }

      logger.log(level, message, meta, callback);
    };

    console.log(`Successfully Started service ${service.name}`);
  },

  stop: () => {
    console.log(`Stopping service ${service.name}`);

    /**
     * de-assign logger from process object.
     */
    process.log = undefined;

    console.log(`Successfully Stopped service ${service.name}`);
  },
};

export default service;
