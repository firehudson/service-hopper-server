import mongoose from 'mongoose';

const computeIdField = lastRecord => (lastRecord ? lastRecord.id + 1 : 0);

/**
 * 
 *  It adds field id to the provided schema.
 * -------------------------------
 * 
 *  Usage -
 *    assignId(targetSchema, targetModelName)
 * 
 *    @params
 *    - targetSchema: mongoose.Schema 
 *    - targetModelName: mongoose.Model
 */

const assignId = (targetSchema, targetModelName) => {
  targetSchema.add({ id: mongoose.SchemaTypes.Number });

  targetSchema.pre('save', function addIdField(next) {
    const targetModel = mongoose.model(targetModelName);

    /**
     * Query target collection's record with highest id.
     */
    targetModel.findOne().sort({ id: -1 })
      .then((lastRecord) => {
        this.id = computeIdField(lastRecord);
        next();
      });
  });
};

export default assignId;
