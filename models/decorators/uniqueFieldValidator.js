import mongoose from 'mongoose';

/**
 *  Mongoose default unique validator does not differenciate between 
 *  null and actual values. So if the field is not required the situation
 *  oftenly occur when a Mongoose raises a validation error for
 *  duplicate record entry.
 * 
 *  ----------------------------------------------
 *    Custom unique field validator for Mongoose
 *  ----------------------------------------------
 * 
 *  Usage -
 *    uniqueFieldValidator(uniqueFields)
 * 
 *    @params
 *    - uniqueFields: [String (field_name)]
 */

const uniqueFieldValidator = uniqueFields => (targetSchema, targetModelName) => (
  targetSchema.pre('save', function addIdField(next) {
    const targetModel = mongoose.model(targetModelName);
    const errors = [];

    uniqueFields.forEach(async (field, index) => {
      const currentfieldValue = this[field];

      if (currentfieldValue) {
        /**
         * find a record with same value from the collection.
         */
        await targetModel.findOne({ [field]: currentfieldValue })
          .then((existingDoc) => {
            if (existingDoc) {
              /**
               * generate custom errors for duplicated record.
               */
              const errorMessage = `found duplicate record with key ${field}`;

              process.log('error', errorMessage, { existingDoc });
              errors.push(new mongoose.Error(errorMessage));
            }
          });

        /**
         * TODO: better way to handle async query and passing errors if any.
         */
        if (index + 1 === uniqueFields.length) {
          /**
           * return the last possible error if available,
           * 
           * mongoose hooks.callback accpets only on argument of type mongoose.Error.
           * if provided, argument is considered as error object and no record is saved.
           */
          next(errors[errors.length - 1]);
        }
      }
    });
  })
);

export default uniqueFieldValidator;
