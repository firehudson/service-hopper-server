import mongoose, { SchemaTypes } from 'mongoose';
import assignId from './decorators/assignId';
import uniqueFieldValidator from './decorators/uniqueFieldValidator';
import decorate from './helpers/decorateModel';

const ServiceCategory = mongoose.model('serviceCategory');
const SERVICE_TYPES = ['hourly', 'metered', 'absolute'];

const serviceSchema = new mongoose.Schema({
  name: {
    type: SchemaTypes.String,
    required: true,
  },
  charges: {
    type: SchemaTypes.Number,
    required: true,
  },
  serviceType: {
    type: SchemaTypes.String,
    enum: SERVICE_TYPES,
    required: true,
  },
  description: {
    type: SchemaTypes.String,
  },
  serviceCategoryId: {
    type: SchemaTypes.Number,
    validate: {
      validator: id => ServiceCategory.findOne({ id }).then(user => user),
      message: 'No Service Category available with id {VALUE}',
    },
  },
  visibleToUser: {
    type: SchemaTypes.Boolean,
    default: false,
  },
});


const generateMongooseError = (id) => {
  const errorMessage = `No service found with id: ${id}`;
  process.log('error', errorMessage);
  return new mongoose.Error(errorMessage);
};


serviceSchema.statics.approve = function approve(id) {
  return this.findOneAndUpdate({ id }, { visibleToUser: true }, { new: true })
    .then((doc) => {
      if (!doc) {
        return generateMongooseError(id);
      }

      return doc;
    })
    .catch(err => err);
};


serviceSchema.statics.remove = function remove(id) {
  return this.findOneAndRemove({ id })
    .then((doc) => {
      if (!doc) {
        return generateMongooseError(id);
      }

      return doc;
    })
    .catch(err => err);
};


const uniqueValidator = uniqueFieldValidator(['name']);

decorate(assignId, uniqueValidator)(serviceSchema, 'service');
