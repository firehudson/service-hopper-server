import mongoose from 'mongoose';
import assignId from './decorators/assignId';
import uniqueFieldValidator from './decorators/uniqueFieldValidator';
import decorate from './helpers/decorateModel';

const USER_ROLES = ['service_provider', 'consumer', 'super_user'];
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const userSchema = new mongoose.Schema({
  contactNumber: {
    type: String,
    required: true,
    maxlength: 10,
    minlength: 10,
    validate: {
      validator: contactNumber => !isNaN(contactNumber),
      message: '{VALUE} is not a valid mobile number',
    },
  },
  dob: Date,
  email: {
    type: String,
    validate: {
      validator: EMAIL_REGEX,
      message: '{VALUE} is not a valid email!',
    },
  },
  firstName: {
    type: String,
    trim: true,
  },
  lastLogin: Date,
  lastName: {
    type: String,
    trim: true,
  },
  password: {
    type: String,
    required: true,
  },
  userName: {
    type: String,
    trim: true,
  },
  userType: {
    type: String,
    enum: USER_ROLES,
    default: USER_ROLES[1],
  },
});

/**
 * User can register himself with minimal details.
 * 
 * In case if personal details are empty/partially-empty,
 * manipulate fullname here.
 * 
 */
userSchema.methods.fullname = function fullname() {
  const firstHalf = `${this.firstName || ''}`;
  const middleHalf = `${this.firstName ? ' ' : ''}`;
  const laterHalf = `${this.lastName || ''}`;

  return `${firstHalf}${middleHalf}${laterHalf}`.trim();
};

/**
 * User can login from multiple browsers at the same time.
 * 
 * for each successful login user will be provided with a
 * refreshToken-accessToken pair, which later can be used for authentication.
 * 
 *
 * if user with provided contactNumber and password doesn't exists,
 * the system will throw an error,
 *
 *   exapmle error
 *    - No reocrd with contactNumber: ${contactNumber} and password ${password}
 */
userSchema.statics.login = function login(contactNumber, password) {
  return this.findOne({ contactNumber, password })
    .then((user) => {
      if (!user) {
        const errorMessage = `No reocrd with contactNumber: ${contactNumber} and password ${password}.`;
        process.log('error', errorMessage);
        throw new mongoose.Error(errorMessage);
      }

      const AuthToken = mongoose.model('authToken');

      return AuthToken.create({ userId: user.id })
        .then(({ accessToken, refreshToken }) => ({
          userName: user.userName,
          fullName: user.fullname(),
          accessToken,
          refreshToken,
        }))
        .catch(err => err);
    })
    .catch(err => err);
};

userSchema.statics.signup = function signup(userObject) {
  if (userObject.userType === 'super_user') {
    const errorMessage = 'You can not signup as super user';
    return Promise.resolve(new mongoose.Error(errorMessage));
  }

  return this.create(userObject);
};

userSchema.statics.logout = function signup(accessToken) {
  const AuthToken = mongoose.model('authToken');
  return AuthToken.findOneAndRemove({ 'accessToken.token': accessToken });
};

const uniqueValidator = uniqueFieldValidator([
  'email',
  'userName',
  'contactNumber',
]);

decorate(assignId, uniqueValidator)(userSchema, 'user');
