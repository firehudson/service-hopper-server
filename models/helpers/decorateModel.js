import mongoose from 'mongoose';

/**
 * 
 * Decorates a model with all provided decorators,
 * and finally registers the schema with provided model name.
 * 
 *  @params
 *    decorators: a function that accepts two parameters,
 *      1) targetModelName
 *      2) targetModelName
 * 
 *  Usage -
 *    decorate(decorator)(schema, "modelName");
 * 
 */

const decorateModel = (...decorators) => (targetSchema, targetModelName) => {
  decorators.forEach((decorator) => {
    decorator(targetSchema, targetModelName);
  });

  mongoose.model(targetModelName, targetSchema);
};

export default decorateModel;
