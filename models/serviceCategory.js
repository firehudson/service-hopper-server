import mongoose, { SchemaTypes } from 'mongoose';
import assignId from './decorators/assignId';
import uniqueFieldValidator from './decorators/uniqueFieldValidator';
import decorate from './helpers/decorateModel';

const serviceCategorySchema = new mongoose.Schema({
  name: {
    type: SchemaTypes.String,
  },
  services: [{
    type: SchemaTypes.Number,
  }],
  visibleToUser: {
    type: SchemaTypes.Boolean,
    default: false,
  },
});

const generateMongooseError = (id) => {
  const errorMessage = `No service-category found with id: ${id}`;
  process.log('error', errorMessage);
  return new mongoose.Error(errorMessage);
};

serviceCategorySchema.statics.approve = function approve(id) {
  return this.findOneAndUpdate({ id }, { visibleToUser: true }, { new: true })
    .then((doc) => {
      if (!doc) {
        return generateMongooseError(id);
      }

      return doc;
    })
    .catch(err => err);
};


serviceCategorySchema.statics.rename = function rename(id, name) {
  return this.findOneAndUpdate({ id }, { name }, { new: true })
    .then((doc) => {
      if (!doc) {
        return generateMongooseError(id);
      }

      return doc;
    })
    .catch(err => err);
};


serviceCategorySchema.statics.remove = function remove(id) {
  return this.findOneAndRemove({ id })
    .then((doc) => {
      if (!doc) {
        return generateMongooseError(id);
      }

      return doc;
    })
    .catch(err => err);
};

const uniqueValidator = uniqueFieldValidator(['name']);

decorate(assignId, uniqueValidator)(serviceCategorySchema, 'serviceCategory');
