import moment from 'moment';
import mongoose, { SchemaTypes } from 'mongoose';
import assignId from './decorators/assignId';
import decorate from './helpers/decorateModel';

const invoiceSchema = new mongoose.Schema({
  serviceId: {
    type: SchemaTypes.Number,
    required: true,
  },
  amount: {
    type: SchemaTypes.Number,
    required: true,
  },
  invoiceDate: {
    type: SchemaTypes.Date,
    default: () => moment(),
  },
  description: {
    type: SchemaTypes.String,
  },
  userId: {
    type: SchemaTypes.Number,
  },
});


decorate(assignId)(invoiceSchema, 'invoice');
