import mongoose from 'mongoose';
import moment from 'moment';
import { encryptKey } from '../utils/cipher';
import assignId from './decorators/assignId';
import decorate from './helpers/decorateModel';

/**
 * 
 * A simple schema to store access tokens.
 * 
 * A user is provided with a set of accessToken and refreshToken.
 * Using accessToken user can access protected content, and if accessToken expires,
 * can request a new one with the provided refreshToken.
 * 
 * A user can login from multiple systems, thus there can be
 * multiple accessToken/refreshToken for a user.
 * 
 */

const refreshTokenLifeTimeDays = 15;
const accessTokenLifeTimeDays = 3;
const convertTime = days => moment().milliseconds(0).add(days, 'days');
const User = mongoose.model('user');

const authTokenSchema = new mongoose.Schema({
  accessToken: {
    token: {
      type: String,
      default: () => encryptKey(convertTime(accessTokenLifeTimeDays).toString()),
    },
    validUpTo: {
      type: Date,
      default: () => convertTime(accessTokenLifeTimeDays),
    },
  },
  refreshToken: {
    token: {
      type: String,
      default: () => encryptKey(convertTime(refreshTokenLifeTimeDays).toString()),
    },
    validUpTo: {
      type: Date,
      default: () => convertTime(refreshTokenLifeTimeDays),
    },
  },
  userId: {
    type: Number,
    required: true,
    validate: {
      validator: id => User.findOne({ id }).then(user => user),
      message: 'No user record available with id {VALUE}',
    },
  },
});

decorate(assignId)(authTokenSchema, 'authToken');
