import express from 'express';
import mongoose from 'mongoose';
import sanitizeError from '../utils/sanitizeError';

const router = express.Router();
const User = mongoose.model('user');

/**
 * create a new user doc and return the saved doc in the response.
 * 
 * params -
 *  - contactNumber [required]
 *  - password [required]
 *  - userName [unique]
 *  - email [unique]
 *  - dob
 *  - firstName
 *  - lastName
 * 
 * 
 *  success response -
 *   {
 *      "id": ###,
 *      "password": "asdf",
 *      "contactNumber": "1234567896",
 *      "userName": "firehudson2",
 *      "dob": "1994-05-01T18:30:00.000Z",
 *      "email": "firehudosn2@gmail.com",
 *      "firstName": "aditya",
 *      "lastName": "vaishnav",
 *      "userType": "consumer"
 *   }
 * 
 * 
 *  error responses -
 *   {
 *      "message": "found duplicate record with key ${parameter-name}",
 *      "name": "MongooseError"
 *   }
 *  or
 *   {
 *      "message": "user validation failed: ${field-name}: ${field-value} is not valid!",
 *      "name": "ValidationError"
 *   }
 *
 */
router.post('/signup', (req, res) => {
  User.signup(req.body)
    .then(doc => res.status(200).send(doc))
    .catch(err => res.status(500).send(err));
});

/**
 * processes user login, generates a refreshToken-accessToken pair and
 * return as response.
 * 
 * params -
 *  - contactNumber [required]
 *  - password [required]
 * 
 * 
 * success response -
 *  {
 *    "userName": "hugli",
 *    "fullName": "Vicky Donar",
 *    "accessToken": {
 *        "token": "09fe841c4e7e67b9dc0666b6719bc10213c1cde0dad0e04f117f3291737dcca5",
 *        "validUpTo": "2017-09-03T21:37:00.000Z"
 *    },
 *    "refreshToken": {
 *        "token": "c0d9cac6781c1b6f7e0fde8e709eaf65eaada3836b7c429b642e3215f25e9eb8",
 *        "validUpTo": "2017-09-15T21:37:00.000Z"
 *    }
 *  }
 * 
 * 
 * error responses -
 *  {
 *   "message": "No reocrd with contactNumber: {contactNumberValue} and password ${passwordValue}.",
 *   "name": "MongooseError"
 *  }
 */
router.post('/login', (req, res) => {
  /**
   * TODO: extract and save ip address (request.connection.remoteAddress) to prevent
   *       multiple assignment of token to same user from same browser.
   */
  User.login(req.body.contactNumber, req.body.password)
    .then((doc) => {
      if (doc instanceof Error) {
        throw sanitizeError(doc);
      }

      res.status(200).send(doc);
    })
    .catch(err => res.status(500).send(err));
});

router.post('/logout', (req, res) => {
  User.logout(req.headers.authorization)
    .then((err) => {
      if (err instanceof Error) {
        throw sanitizeError(err);
      }

      const response = {
        status: 200,
        title: 'user logged out successfully',
        details: {
          id: err.id,
          authToken: err,
          removed: true,
        },
      };

      res.status(200).send(response);
    })
    .catch(err => res.status(500).send(err));
});

export default router;
