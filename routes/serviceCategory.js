import express from 'express';
import mongoose from 'mongoose';
import sanitizeError from '../utils/sanitizeError';

const router = express.Router();
const ServiceCategory = mongoose.model('serviceCategory');

/**
 *  returns list of all service categories.
 * 
 *  requestUrl - `/serviceCategory`
 *  method - GET
 *
 *  success response -
 *   [
 *     {
 *       "id": ###,
 *       "name": "house keeping",
 *       "visibleToUser": false,
 *       "services": []
 *     },
 *     {
 *       "id": ###,
 *       "name": "new service",
 *       "visibleToUser": false,
 *       "services": []
 *     }
 *   ]
 */
router.get('/', (req, res) => {
  ServiceCategory.find({ visibleToUser: true })
    .then(doc => res.status(200).send(doc))
    .catch(err => res.status(500).send(err));
});


/**
 *  returns service categories by id.
 * 
 *  requestUrl - `/serviceCategory/${id}`
 *  method - GET
 * 
 *  success response -
 *   [
 *     {
 *       "id": ###,
 *       "name": "house keeping",
 *       "visibleToUser": false,
 *       "services": []
 *     },
 *     {
 *       "id": ###,
 *       "name": "new service",
 *       "visibleToUser": false,
 *       "services": []
 *     }
 *   ]
 */
router.get('/:id', (req, res) => {
  ServiceCategory.findOne({ id: req.params.id, visibleToUser: true })
    .then(doc => res.status(200).send(doc))
    .catch(err => res.status(500).send(err));
});

/**
 * create a new service-category doc and return the saved doc in the response.
 * 
 * requestUrl - `/serviceCategory/create`
 * method - POST
 * params -
 *  - name [required]
 * 
 *  success response -
 *   {
 *       "id": ###,
 *       "name": "house keeping",
 *       "visibleToUser": false,
 *       "services": []
 *   }
 * 
 *  error responses -
 *   {
 *      "message": "found duplicate record with key ${parameter-name}",
 *      "name": "MongooseError"
 *   }
 *  or
 *   {
 *      "message": "user authentication failed",
 *      "name": "Error"
 *   }
 */
router.post('/create', (req, res) => {
  const serviceCategory = new ServiceCategory({ name: req.body.name });
  const canCreateCategory = req.user.userType === 'super_user' || req.user.userType === 'service_provider';

  if (canCreateCategory) {
    serviceCategory.save()
      .then(doc => res.status(200).send(doc))
      .catch(err => res.status(500).send(err));
  } else {
    const response = {
      status: 500,
      title: 'unauthorized access',
      meta: {
        user: req.user,
        serviceCategory: serviceCategory.toObject(),
      },
    };

    res.status(500).send(response);
  }
});

/**
 * As a service-category is create by service-provider it is initially not
 * visible to user. A super-user has to approve a service-category in order to make
 * it visible to other.
 * 
 * requestUrl - `/serviceCategory/approve/`
 * method - POST
 * params -
 *  - id [required]
 * 
 *  success response -
 *   {
 *       "id": ###,
 *       "name": "house keeping",
 *       "visibleToUser": true,
 *       "services": []
 *   }
 * 
 *  error responses -
 *   {
 *       "message": "No service-category found with id: ###",
 *       "name": "MongooseError"
 *   }
 *  or
 *   {
 *      "message": "user authentication failed",
 *      "name": "Error"
 *   }
 */
router.post('/approve', (req, res) => {
  const isSuperUser = req.user.userType === 'super_user';

  if (isSuperUser) {
    ServiceCategory.approve(req.body.id)
      .then((doc) => {
        if (sanitizeError(doc)) {
          throw sanitizeError(doc);
        }

        res.status(200).send(doc);
      })
      .catch(err => res.status(500).send(err));
  } else {
    const response = {
      status: 500,
      title: 'unauthorized access',
      meta: {
        user: req.user,
      },
      description: 'only super_user can approve a service category',
    };

    res.status(500).send(response);
  }
});


/**
 * API for super user and service provider to delete a service category.
 * 
 * requestUrl - `/serviceCategory/delete`
 * method - POST
 * params -
 *  - id [required]
 * 
 * 
 *  success response -
 *   {
 *     {
 *       "id": ###,
 *       "name": "house keeping",
 *       "visibleToUser": true,
 *       "services": []
 *     },
 *     removed: true
 *   }
 * 
 * 
 *  error responses -
 *   {
 *       "message": "No service-category found with id: ###",
 *       "name": "MongooseError"
 *   }
 *  or
 *   {
 *      "message": "user authentication failed",
 *      "name": "Error"
 *   }
 */
router.post('/delete', (req, res) => {
  const canDeleteCategory = req.user.userType === 'super_user' || req.user.userType === 'service_provider';

  if (canDeleteCategory) {
    ServiceCategory.remove(req.body.id)
      .then((doc) => {
        if (sanitizeError(doc)) {
          throw sanitizeError(doc);
        }

        res.status(200).send({ doc, removed: true });
      })
      .catch(err => res.status(500).send(err));
  } else {
    const response = {
      status: 500,
      title: 'unauthorized access',
      meta: {
        user: req.user,
      },
      description: 'only super_user can delete a service category',
    };

    res.status(500).send(response);
  }
});

/**
 * API for super user and service provider to rename a service category.
 * 
 * requestUrl - `/serviceCategory/rename`
 * method - POST
 * params -
 *  - id [required]
 *  - name [required]
 * 
 * 
 *  success response -
 *   {
 *     {
 *       "id": ###,
 *       "name": "updated house keeping",
 *       "visibleToUser": true,
 *       "services": []
 *     },
 *     renamed: true
 *   }
 * 
 * 
 *  error responses -
 *   {
 *       "message": "No service-category found with id: ###",
 *       "name": "MongooseError"
 *   }
 *  or
 *   {
 *      "message": "user authentication failed",
 *      "name": "Error"
 *   }
 */
router.post('/rename', (req, res) => {
  const canRenameCategory = req.user.userType === 'super_user' || req.user.userType === 'service_provider';

  if (canRenameCategory) {
    ServiceCategory.rename(req.body.id, req.body.name)
      .then((doc) => {
        if (sanitizeError(doc)) {
          throw sanitizeError(doc);
        }

        res.status(200).send({ doc, renamed: true });
      })
      .catch(err => res.status(500).send(err));
  } else {
    const response = {
      status: 500,
      title: 'unauthorized access',
      meta: {
        user: req.user,
      },
      description: 'only super_user can rename a service category',
    };

    res.status(500).send(response);
  }
});

export default router;
