import express from 'express';

const router = express.Router();

/* GET healthcheck */
router.get('/', (req, res) => {
  res.send(`Server is up at localhost:${process.env.PORT}`);
});

/* GET healthcheck sentry */
router.get('/sentry', () => {
  // throws a new error, See sentry for stack-trace.
  throw new Error('Haha!, Something messed up!');
});

router.get('/winston', (req, res) => {
  // log info using winston.
  process.log('info', 'this is just a info with metadata', { meta: 'data' });
  process.log('warn', 'this is just a warn', () => console.log('Callback after logging.'));
  process.log('error', 'this is just a error');

  res.send('Success');
});

export default router;
