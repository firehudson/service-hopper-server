import express from 'express';
import mongoose from 'mongoose';
import generateResourceRoutes, { GetAll, GetOneById } from '../utils/generateResourceRoutes';
import sanitizeError from '../utils/sanitizeError';

const router = express.Router();
const Service = mongoose.model('service');

const queryConfig = {
  [GetOneById]: { visibleToUser: true },
  [GetAll]: { visibleToUser: true },
};

generateResourceRoutes(router, 'service', queryConfig);


/**
 * create a new service doc and return the saved doc in the response.
 * 
 * requestUrl - `/service/create`
 * method - POST
 * params -
 *  - name [required]
 *  - charges [required]
 *  - serviceType [required]
 *  - description
 *  - serviceCategoryId
 *
 *  success response -
 *   {
 *       "id": ###,
 *       "name": "pest control",
 *       "visibleToUser": false,
 *   }
 * 
 *  error responses -
 *   {
 *      "message": "found duplicate record with key ${parameter-name}",
 *      "name": "MongooseError"
 *   }
 *  or
 *   {
 *      "message": "user authentication failed",
 *      "name": "Error"
 *   }
 */
router.post('/create', (req, res) => {
  const isSuperUser = req.user.userType === 'super_user';
  const isServiceProvider = req.user.userType === 'service_provider';

  if (isSuperUser || isServiceProvider) {
    const service = new Service({
      charges: req.body.charges,
      description: req.body.description || '',
      name: req.body.name,
      serviceCategoryId: req.body.serviceCategoryId,
      serviceType: req.body.serviceType,
      visibleToUser: isSuperUser,
    });

    service.save()
      .then(doc => res.status(200).send(doc))
      .catch(err => res.status(500).send(err));
  } else {
    const response = {
      status: 500,
      title: 'unauthorized access',
      meta: {
        user: req.user,
      },
    };

    res.status(500).send(response);
  }
});

/**
 * As a service is create by service-provider it is initially not
 * visible to user. A super-user has to approve a service in order to make
 * it visible to other.
 * 
 * requestUrl - `/service/approve/`
 * method - POST
 * params -
 *  - id [required]
 * 
 *  success response -
 *   {
 *       "id": ###,
 *       "name": "pest control",
 *        ......
 *        ......
 *       "visibleToUser": true,
 *   }
 * 
 *  error responses -
 *   {
 *       "message": "No service found with id: ###",
 *       "name": "MongooseError"
 *   }
 *  or
 *   {
 *      "message": "user authentication failed",
 *      "name": "Error"
 *   }
 */
router.post('/approve', (req, res) => {
  const isSuperUser = req.user.userType === 'super_user';

  if (isSuperUser) {
    Service.approve(req.body.id)
      .then((doc) => {
        if (sanitizeError(doc)) {
          throw sanitizeError(doc);
        }

        res.status(200).send(doc);
      })
      .catch(err => res.status(500).send(err));
  } else {
    const response = {
      status: 500,
      title: 'unauthorized access',
      meta: {
        user: req.user,
      },
      description: 'only super_user can approve a service',
    };

    res.status(500).send(response);
  }
});


/**
 * API for super user and service provider to delete a service.
 * 
 * requestUrl - `/service/delete`
 * method - POST
 * params -
 *  - id [required]
 * 
 * 
 *  success response -
 *   {
 *     {
 *       "id": ###,
 *       "name": "pest control",
 *        ......
 *        ......
 *       "visibleToUser": true,
 *     },
 *     removed: true
 *   }
 * 
 * 
 *  error responses -
 *   {
 *       "message": "No service found with id: ###",
 *       "name": "MongooseError"
 *   }
 *  or
 *   {
 *      "message": "user authentication failed",
 *      "name": "Error"
 *   }
 */
router.post('/delete', (req, res) => {
  const canDeleteService = req.user.userType === 'super_user' || req.user.userType === 'service_provider';

  if (canDeleteService) {
    Service.remove(req.body.id)
      .then((doc) => {
        if (sanitizeError(doc)) {
          throw sanitizeError(doc);
        }

        res.status(200).send({ doc, removed: true });
      })
      .catch(err => res.status(500).send(err));
  } else {
    const response = {
      status: 500,
      title: 'unauthorized access',
      meta: {
        user: req.user,
      },
      description: 'only super_user can delete a service',
    };

    res.status(500).send(response);
  }
});


/**
 * API for super user and service provider to update a service.
 * 
 * requestUrl - `/service/update`
 * method - POST
 * params -
 *  - id [required]
 *  - updatedService | Object [required]
 * 
 * 
 *  success response -
 *   {
 *     {
 *       "id": ###,
 *       "name": "updated house keeping",
 *       "visibleToUser": true,
 *     },
 *     updated: true
 *   }
 * 
 * 
 *  error responses -
 *   {
 *       "message": "No service found with id: ###",
 *       "name": "MongooseError"
 *   }
 *  or
 *   {
 *      "message": "user authentication failed",
 *      "name": "Error"
 *   }
 */
router.post('/update', (req, res) => {
  const canUpdate = req.user.userType === 'super_user' || req.user.userType === 'service_provider';

  if (canUpdate) {
    Service.findOneAndUpdate({ id: req.body.id }, req.body.updatedService, { new: true })
      .then((doc) => {
        res.status(200).send({ doc, updated: true });
      })
      .catch(err => res.status(500).send(err));
  } else {
    const response = {
      status: 500,
      title: 'unauthorized access',
      meta: {
        user: req.user,
      },
      description: 'only super_user or service_provider can update a service',
    };

    res.status(500).send(response);
  }
});

export default router;
