import express from 'express';

const router = express.Router();

/* GET healthcheck */
router.get('/', (req, res) => {
  res.send(`Server is up at localhost:${process.env.PORT}`);
});

export default router;
