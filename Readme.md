## Service Hopper
Expressjs server for Service hopper

### Steps to preceed with development

#### Starting Server
- move to root directory of your project.
- `npm install | yarn`.
- `npm start | yarn start`.
- verify server is running on port defined in .env file.

#### API DOC
Please refer to [API DOC](https://documenter.getpostman.com/collection/view/2714454-f876332d-eea4-ec00-976b-189d18535c78)
