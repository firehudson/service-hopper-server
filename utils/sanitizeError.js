import mongoose from 'mongoose';

/**
 * Error objects can not be sent as response from the server.
 * this utility parse recoginzed error formats to JSON.
 *
 * @param {Error} error - some error that needs to be sanitized.
 */
const sanitizeError = (error) => {
  let sanitizedError;

  /**
   * Standard JavaScript Error
   */
  if (error instanceof Error || error instanceof mongoose.Error) {
    sanitizedError = {
      message: error.message,
      name: error.name,
    };
  }

  return sanitizedError;
};

export default sanitizeError;
