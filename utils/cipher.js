import crypto from 'crypto';

const SECRET_KEY = process.env.CIPHER_SECRET;

export const encryptKey = key => crypto
  .createCipher('aes-128-cbc', SECRET_KEY)
  .update(key, 'utf8', 'hex');

export const decryptKey = key => crypto
  .createDecipher('aes-128-cbc', SECRET_KEY)
  .update(key, 'hex', 'utf8');
