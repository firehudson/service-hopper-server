
/**
 * -------------------------------------------
 *   API Parameter Validation configurations
 * -------------------------------------------
 * 
 * Constant valid parameters declarations format
 * 
 * format - key: [validParam]
 *  - key: METHOD_NAME:API_END_POINT
 *  - value: array of valid parameters
 * 
 */

const paramValidations = {
  'POST:/users/signup': [
    'contactNumber',
    'dob',
    'email',
    'firstName',
    'lastLogin',
    'lastName',
    'password',
    'userName',
    'userType',
  ],
  'POST:/serviceCategory/create': [
    'name',
  ],
  'POST:/serviceCategory/approve': [
    'id',
  ],
  'POST:/serviceCategory/delete': [
    'id',
  ],
  'POST:/serviceCategory/rename': [
    'id',
    'name',
  ],
};

export default paramValidations;
