import mongoose from 'mongoose';
import _ from 'lodash';

export const GetAll = Symbol('GetAll');
export const GetOneById = Symbol('GetOneById');

const generateResourceRoutes = (targetRouter, resourceModelName, queryConfig = {}) => {
  const targetModel = mongoose.model(resourceModelName);

  targetRouter.get('/', (req, res) => {
    targetModel.find(queryConfig[GetAll] || {})
      .then(doc => res.status(200).send(doc))
      .catch(err => res.status(500).send(err));
  });

  targetRouter.get('/:id', (req, res) => {
    const conditions = _.merge({ id: req.params.id }, queryConfig[GetOneById]);
    targetModel.findOne(conditions)
      .then(doc => res.status(200).send(doc))
      .catch(err => res.status(500).send(err));
  });
};

export default generateResourceRoutes;
