
const base = {
  LOGGING: true,
  SENTRY_DSN: 'https://63db928f7bec4587b01011931b0ef5f5:aacd282f22c1480a92d49d4aee93a9ff@sentry.io/206016',
  LOG_FILE_PATH: 'logs/debug-log',
  CIPHER_SECRET: 'sandBoxSecretKey',
};

const development = {
  PORT: 4000,
  MONGO_DB_URL: 'mongodb://localhost/service-hopper',
};

const production = {
  MONGO_DB_URL: 'mongodb://firehudson:firehudson@ds111876.mlab.com:11876/service_hopper',
};

const config = Object.assign({},
  base,
  process.env.NODE_ENV === 'development' ? development : production,
);

module.exports = () => {
  Object.keys(config).forEach((key) => {
    process.env[key] = config[key];
  });
};
