import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import raven from 'raven';
import fs from 'fs';
import cors from 'cors';

import validateParams from './middlewares/validateParams';
import extractUserFromRequest from './middlewares/extractUserFromRequest';

import index from './routes/index';
import healthcheck from './routes/healthcheck';
import users from './routes/user';
import serviceCategory from './routes/serviceCategory';
import service from './routes/service';

if (!fs.existsSync(path.join(__dirname, 'logs'))) {
  fs.mkdirSync(path.join(__dirname, 'logs'));
}

if (!fs.existsSync(path.join(__dirname, 'logs', 'debug-log'))) {
  fs.closeSync(fs.openSync(path.join(__dirname, 'logs', 'debug-log'), 'w'));
}

const app = express();

// configure Sentry for error tracking
raven.config(process.env.SENTRY_DSN).install();
app.use(raven.requestHandler());

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));

app.use(validateParams);
app.use(extractUserFromRequest);

app.use('/', index);
app.use('/healthcheck', healthcheck);
app.use('/users', users);
app.use('/serviceCategory', serviceCategory);
app.use('/service', service);

app.use(raven.errorHandler());

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send({ error: 'some error occured', errorId: res.sentry });
});

export default app;
