import _ from 'lodash';
import mongoose from 'mongoose';
import sanitizeError from '../utils/sanitizeError';

/**
 * list of URLs that needs authentication.
 */
const authorizedUrls = [
  'POST:/users/logout',
  'POST:/serviceCategory/create',
  'POST:/serviceCategory/approve',
  'POST:/serviceCategory/delete',
  'POST:/serviceCategory/rename',
  'POST:/service/create',
  'POST:/service/approve',
  'POST:/service/delete',
  'POST:/service/update',
];

/**
 * A basic user-authentication middleware.
 * 
 * Any restricted resource should be available to use for unauthenticated user.
 * This middleware try to extract accessToken from request headers and
 * finds a user to which this accessToken is assigned.
 * If found, user is added with the request object and passed for further processing.
 */
const extractUserFromRequest = (req, res, next) => { // eslint-disable-line consistent-return
  const AuthToken = mongoose.model('authToken');
  const User = mongoose.model('user');
  const accessToken = req.headers.authorization;

  if (!_.includes(authorizedUrls, `${req.method}:${req.originalUrl}`)) {
    return next();
  }

  AuthToken.findOne({ 'accessToken.token': accessToken }, { userId: true, id: true })
    .then((authToken) => {
      if (!(authToken && typeof authToken.userId !== 'undefined')) {
        const errorMessage = 'Authentication Error, please login again';
        process.log('error', errorMessage, { accessToken });
        throw sanitizeError(new Error(errorMessage));
      }

      User.findOne({ id: authToken.userId })
        .then((user) => {
          if (!user) {
            const errorMessage = `Error authenticating user, No user present with id ${authToken.userId}`;
            process.log('error', errorMessage, { accessToken });
            throw sanitizeError(new Error(errorMessage));
          }

          req.user = user;
          process.log('info', 'user authentication successful.', { user });
          next();
        })
        .catch(err => res.status(500).send(err));
    })
    .catch(err => res.status(500).send(err));
};

export default extractUserFromRequest;
