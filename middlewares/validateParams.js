import _ from 'lodash';
import validations from '../utils/validations';

const validateParams = (req, res, next) => {
  const urlKey = `${req.method}:${req.originalUrl}`;
  const validParams = validations[urlKey];

  if (!validParams) {
    /**
     * permit API call if no validations found for specific route.
     */
    next();
  } else {
    _.mapKeys(req.body, (paramValue, paramKey) => {
      const isValid = _.includes(validParams, paramKey);

      if (!isValid) {
        next(new Error(`[Validation Error]: unpermitted param ${paramKey}`));
      }
    });

    next();
  }
};

export default validateParams;
